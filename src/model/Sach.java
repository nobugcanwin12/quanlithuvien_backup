package model;

public class Sach {

    private int id;
    private String ten;
    private int maTacGia;
    private int maTheLoai;
    private int maNXB;
    private int namXB;
    private int maTienMuon;
    private int trangThai;

    public Sach() {
    }

    public Sach(int id, String ten, int maTacGia, int maTheLoai, int maNXB, int namXB, int maTienMuon, int trangThai) {
        this.id = id;
        this.ten = ten;
        this.maTacGia = maTacGia;
        this.maTheLoai = maTheLoai;
        this.maNXB = maNXB;
        this.namXB = namXB;
        this.maTienMuon = maTienMuon;
        this.trangThai = trangThai;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getMaTacGia() {
        return maTacGia;
    }

    public void setMaTacGia(int maTacGia) {
        this.maTacGia = maTacGia;
    }

    public int getMaTheLoai() {
        return maTheLoai;
    }

    public void setMaTheLoai(int maTheLoai) {
        this.maTheLoai = maTheLoai;
    }

    public int getMaNXB() {
        return maNXB;
    }

    public void setMaNXB(int maNXB) {
        this.maNXB = maNXB;
    }

    public int getNamXB() {
        return namXB;
    }

    public void setNamXB(int namXB) {
        this.namXB = namXB;
    }

    public int getMaTienMuon() {
        return maTienMuon;
    }

    public void setMaTienMuon(int maTienMuon) {
        this.maTienMuon = maTienMuon;
    }

    public int getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(int trangThai) {
        this.trangThai = trangThai;
    }

    
}
