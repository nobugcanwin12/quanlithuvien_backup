package service;

import dao.Dao_23;
import java.sql.SQLException;
import java.util.List;
import model.Sach;

public class Service_23 {

    Dao_23 sachDao = null;

    public Service_23() {
        sachDao = new Dao_23();
    }

    public List<Sach> getDSSach() {
        return sachDao.getDSSach();
    }

    public List<Sach> getBookByTitle(String tenSach) throws SQLException {
        return sachDao.getBookByTitle(tenSach);
    }

    public List<Sach> getTheLoaiSach(String type) {
        return sachDao.getTheLoaiSach(type);
    }
    
    public List<String> getAllTheLoai_16() {
        return sachDao.getAllTheLoai_16();
    }
    
    public String getTrangThaiByMaTrangThai(int mtt) {
        return sachDao.getTrangThaiByMaTrangThai(mtt);
    }
}
