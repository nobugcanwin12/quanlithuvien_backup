-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 02, 2022 lúc 04:08 PM
-- Phiên bản máy phục vụ: 10.4.22-MariaDB
-- Phiên bản PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `quanlithuvien`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chi_tiet_muon_tra`
--

CREATE TABLE `chi_tiet_muon_tra` (
  `ma_mtr` int(11) NOT NULL,
  `id_sach` int(11) NOT NULL,
  `ngay_tra` date DEFAULT NULL,
  `tien_thanh_toan` double DEFAULT NULL,
  `thoi_gian_muon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chi_tiet_muon_tra`
--

INSERT INTO `chi_tiet_muon_tra` (`ma_mtr`, `id_sach`, `ngay_tra`, `tien_thanh_toan`, `thoi_gian_muon`) VALUES
(1, 2, NULL, NULL, 30),
(1, 5, NULL, NULL, 30),
(2, 7, NULL, NULL, 30),
(3, 3, NULL, NULL, 30),
(3, 4, NULL, NULL, 30),
(3, 8, NULL, NULL, 30),
(4, 6, NULL, NULL, 30);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chuc_vu`
--

CREATE TABLE `chuc_vu` (
  `ma_chuc_vu` int(11) NOT NULL,
  `ten_chuc_vu` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chuc_vu`
--

INSERT INTO `chuc_vu` (`ma_chuc_vu`, `ten_chuc_vu`) VALUES
(1, 'Nhân viên'),
(2, 'Chủ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `doc_gia`
--

CREATE TABLE `doc_gia` (
  `ma_doc_gia` int(11) NOT NULL,
  `ten_doc_gia` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dia_chi` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `sdt` varchar(15) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `doc_gia`
--

INSERT INTO `doc_gia` (`ma_doc_gia`, `ten_doc_gia`, `dia_chi`, `sdt`) VALUES
(1, 'Đặng Văn Độc Giả', 'nhà', '0123456789'),
(2, 'Nguyễn Đắc Độc Giả', 'nhà', '0123456789'),
(3, 'Lưu Văn Độc Giả', 'nhà', '0123456789'),
(4, 'Bùi Thanh Độc giả', 'nhà', '0123456789');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `muon_tra`
--

CREATE TABLE `muon_tra` (
  `ma_mtr` int(11) NOT NULL,
  `so_the` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ma_nv` int(11) NOT NULL,
  `ngay_muon` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `muon_tra`
--

INSERT INTO `muon_tra` (`ma_mtr`, `so_the`, `ma_nv`, `ngay_muon`) VALUES
(1, 'dvdg.1', 1, '2022-04-27'),
(2, 'nddg.1', 1, '2022-04-29'),
(3, 'lvdg.1', 1, '2022-04-29'),
(4, 'btdg.1', 1, '2022-04-29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nguoi_dung`
--

CREATE TABLE `nguoi_dung` (
  `ma_nd` int(11) NOT NULL,
  `ten_nd` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ma_chuc_vu` int(11) NOT NULL,
  `tai_khoan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mat_khau` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_sinh` date DEFAULT NULL,
  `sdt` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trang_thai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nguoi_dung`
--

INSERT INTO `nguoi_dung` (`ma_nd`, `ten_nd`, `ma_chuc_vu`, `tai_khoan`, `mat_khau`, `ngay_sinh`, `sdt`, `trang_thai`) VALUES
(1, 'Nguyễn Nhân Viên', 1, 'nhanvien', 'nhanvien', NULL, NULL, 1),
(2, 'Nguyễn Quản Trị', 2, 'admin', 'admin', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nha_xuat_ban`
--

CREATE TABLE `nha_xuat_ban` (
  `ma_nxb` int(11) NOT NULL,
  `ten_nxb` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nha_xuat_ban`
--

INSERT INTO `nha_xuat_ban` (`ma_nxb`, `ten_nxb`) VALUES
(1, 'Kim Đồng'),
(2, 'Tuổi trẻ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sach`
--

CREATE TABLE `sach` (
  `id_sach` int(11) NOT NULL,
  `ten_sach` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ma_tac_gia` int(11) NOT NULL,
  `ma_the_loai` int(11) NOT NULL,
  `ma_nxb` int(11) NOT NULL,
  `nam_xuat_ban` int(5) NOT NULL,
  `ma_tien_muon` int(11) NOT NULL,
  `trang_thai` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sach`
--

INSERT INTO `sach` (`id_sach`, `ten_sach`, `ma_tac_gia`, `ma_the_loai`, `ma_nxb`, `nam_xuat_ban`, `ma_tien_muon`, `trang_thai`) VALUES
(1, 'Tôi Đã Kiếm Được 2.000.000 Đô-La Từ Thị Trường Chứng Khoán Như Thế Nào?', 21, 6, 2, 2004, 1, 2),
(2, 'Bách Thủ Thư Sinh', 12, 1, 1, 2001, 1, 1),
(3, 'Phi Đao Túy Nguyệt', 2, 1, 2, 2002, 1, 1),
(4, 'Machine Learning Cơ Bản', 3, 2, 1, 2003, 1, 1),
(5, 'Laravel 5 Cookbook Enhance Your Amazing Applications', 4, 2, 2, 2004, 1, 1),
(6, 'Pro ASP.NET MVC 5', 5, 2, 1, 2001, 1, 1),
(7, 'Linux All-In-One For Dummies – 5Th Edition', 6, 2, 2, 2002, 1, 1),
(8, 'Cách Học Ngoại Ngữ Nhanh Và Không Bao Giờ Quên', 10, 3, 1, 2003, 1, 1),
(10, 'Phàm Nhân Tu Tiên Chi Tiên Giới Thiên (Phàm Nhân Tu Tiên 2)', 14, 1, 2, 2004, 1, 2),
(11, 'Hồ Nữ', 15, 1, 1, 2001, 1, 2),
(12, 'Văn Thuyết', 16, 1, 2, 2002, 1, 2),
(13, 'Đấu La Đại Lục', 17, 1, 1, 2003, 1, 2),
(14, 'Âm Dương Miện', 17, 1, 2, 2004, 1, 2),
(15, 'Để xây dựng doanh nghiệp hiệu quả', 18, 6, 1, 2001, 1, 2),
(16, '1001 Cách Giữ Chân Khách Hàng', 19, 6, 2, 2002, 1, 2),
(17, 'Cẩm Nang Sale Bất Động Sản Alibaba – Nguyễn Thái Luyện', 20, 6, 1, 2003, 1, 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tac_gia`
--

CREATE TABLE `tac_gia` (
  `ma_tac_gia` int(11) NOT NULL,
  `ten_tac_gia` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tac_gia`
--

INSERT INTO `tac_gia` (`ma_tac_gia`, `ten_tac_gia`) VALUES
(1, 'Nhiều tác giả'),
(2, 'Ức Vân'),
(3, 'Vũ Hữu Tiệp'),
(4, 'Nathan Wu'),
(5, 'Adam Freeman'),
(6, 'Emmett Dulaney'),
(7, 'Nguyễn Thị Hà Bắc'),
(8, 'Jung min kyung'),
(9, 'Lê Huy Khoa'),
(10, 'Gabriel Wyner'),
(11, 'Đặng Trung Ngọc'),
(12, 'Ngọa Long Sinh'),
(14, 'Vong Ngữ'),
(15, 'Dịch Ngũ'),
(16, 'Hạnh Dao Vị Vãn'),
(17, 'Đường Gia Tam Thiếu'),
(18, 'Michael E. Gerber'),
(19, 'Nhất Ly'),
(20, 'Nguyễn Thái Luyện'),
(21, 'Nicolas Darvas');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `the`
--

CREATE TABLE `the` (
  `so_the` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ma_doc_gia` int(11) NOT NULL,
  `ngay_bd` date NOT NULL DEFAULT current_timestamp(),
  `ngay_kt` date NOT NULL,
  `trang_thai_the` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `the`
--

INSERT INTO `the` (`so_the`, `ma_doc_gia`, `ngay_bd`, `ngay_kt`, `trang_thai_the`) VALUES
('btdg.1', 4, '2022-04-29', '2022-05-29', 1),
('dvdg.1', 1, '2022-04-27', '2022-05-29', 1),
('lvdg.1', 3, '2022-04-29', '2022-05-29', 1),
('nddg.1', 2, '2022-04-29', '2022-05-29', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `the_loai`
--

CREATE TABLE `the_loai` (
  `ma_the_loai` int(11) NOT NULL,
  `ten_the_loai` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `the_loai`
--

INSERT INTO `the_loai` (`ma_the_loai`, `ten_the_loai`) VALUES
(1, 'Kiếm hiệp - Tiên hiệp'),
(2, 'Công nghệ thông tin'),
(3, 'Học Ngoại Ngữ'),
(6, 'Kinh Tế - Quản Lý');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tien_muon_sach`
--

CREATE TABLE `tien_muon_sach` (
  `ma_tien_muon` int(11) NOT NULL,
  `so_tien` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tien_muon_sach`
--

INSERT INTO `tien_muon_sach` (`ma_tien_muon`, `so_tien`) VALUES
(1, 20000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `trang_thai_sach`
--

CREATE TABLE `trang_thai_sach` (
  `ma_tt` int(2) NOT NULL,
  `ten_tt` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `trang_thai_sach`
--

INSERT INTO `trang_thai_sach` (`ma_tt`, `ten_tt`) VALUES
(1, 'Đang rảnh'),
(2, 'Đang cho mượn'),
(3, 'Đang bảo trì');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `chi_tiet_muon_tra`
--
ALTER TABLE `chi_tiet_muon_tra`
  ADD PRIMARY KEY (`ma_mtr`,`id_sach`),
  ADD KEY `ma_mtr` (`ma_mtr`),
  ADD KEY `id_sach` (`id_sach`);

--
-- Chỉ mục cho bảng `chuc_vu`
--
ALTER TABLE `chuc_vu`
  ADD PRIMARY KEY (`ma_chuc_vu`);

--
-- Chỉ mục cho bảng `doc_gia`
--
ALTER TABLE `doc_gia`
  ADD PRIMARY KEY (`ma_doc_gia`);

--
-- Chỉ mục cho bảng `muon_tra`
--
ALTER TABLE `muon_tra`
  ADD PRIMARY KEY (`ma_mtr`),
  ADD KEY `so_the` (`so_the`),
  ADD KEY `ma_nv` (`ma_nv`);

--
-- Chỉ mục cho bảng `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  ADD PRIMARY KEY (`ma_nd`),
  ADD KEY `ma_chuc_vu` (`ma_chuc_vu`);

--
-- Chỉ mục cho bảng `nha_xuat_ban`
--
ALTER TABLE `nha_xuat_ban`
  ADD PRIMARY KEY (`ma_nxb`);

--
-- Chỉ mục cho bảng `sach`
--
ALTER TABLE `sach`
  ADD PRIMARY KEY (`id_sach`),
  ADD KEY `ma_tac_gia` (`ma_tac_gia`),
  ADD KEY `ma_the_loai` (`ma_the_loai`),
  ADD KEY `ma_nxb` (`ma_nxb`),
  ADD KEY `ma_tien_muon` (`ma_tien_muon`),
  ADD KEY `trang_thai` (`trang_thai`);

--
-- Chỉ mục cho bảng `tac_gia`
--
ALTER TABLE `tac_gia`
  ADD PRIMARY KEY (`ma_tac_gia`);

--
-- Chỉ mục cho bảng `the`
--
ALTER TABLE `the`
  ADD PRIMARY KEY (`so_the`),
  ADD KEY `ma_doc_gia` (`ma_doc_gia`);

--
-- Chỉ mục cho bảng `the_loai`
--
ALTER TABLE `the_loai`
  ADD PRIMARY KEY (`ma_the_loai`);

--
-- Chỉ mục cho bảng `tien_muon_sach`
--
ALTER TABLE `tien_muon_sach`
  ADD PRIMARY KEY (`ma_tien_muon`);

--
-- Chỉ mục cho bảng `trang_thai_sach`
--
ALTER TABLE `trang_thai_sach`
  ADD PRIMARY KEY (`ma_tt`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `chuc_vu`
--
ALTER TABLE `chuc_vu`
  MODIFY `ma_chuc_vu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `doc_gia`
--
ALTER TABLE `doc_gia`
  MODIFY `ma_doc_gia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `muon_tra`
--
ALTER TABLE `muon_tra`
  MODIFY `ma_mtr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  MODIFY `ma_nd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `nha_xuat_ban`
--
ALTER TABLE `nha_xuat_ban`
  MODIFY `ma_nxb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `sach`
--
ALTER TABLE `sach`
  MODIFY `id_sach` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `tac_gia`
--
ALTER TABLE `tac_gia`
  MODIFY `ma_tac_gia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `the_loai`
--
ALTER TABLE `the_loai`
  MODIFY `ma_the_loai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `tien_muon_sach`
--
ALTER TABLE `tien_muon_sach`
  MODIFY `ma_tien_muon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `trang_thai_sach`
--
ALTER TABLE `trang_thai_sach`
  MODIFY `ma_tt` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `chi_tiet_muon_tra`
--
ALTER TABLE `chi_tiet_muon_tra`
  ADD CONSTRAINT `chi_tiet_muon_tra_ibfk_1` FOREIGN KEY (`ma_mtr`) REFERENCES `muon_tra` (`ma_mtr`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chi_tiet_muon_tra_ibfk_2` FOREIGN KEY (`id_sach`) REFERENCES `sach` (`id_sach`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `muon_tra`
--
ALTER TABLE `muon_tra`
  ADD CONSTRAINT `muon_tra_ibfk_1` FOREIGN KEY (`so_the`) REFERENCES `the` (`so_the`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `muon_tra_ibfk_2` FOREIGN KEY (`ma_nv`) REFERENCES `nguoi_dung` (`ma_nd`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  ADD CONSTRAINT `nguoi_dung_ibfk_1` FOREIGN KEY (`ma_chuc_vu`) REFERENCES `chuc_vu` (`ma_chuc_vu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `sach`
--
ALTER TABLE `sach`
  ADD CONSTRAINT `sach_ibfk_1` FOREIGN KEY (`ma_the_loai`) REFERENCES `the_loai` (`ma_the_loai`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sach_ibfk_2` FOREIGN KEY (`ma_tac_gia`) REFERENCES `tac_gia` (`ma_tac_gia`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sach_ibfk_3` FOREIGN KEY (`ma_nxb`) REFERENCES `nha_xuat_ban` (`ma_nxb`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sach_ibfk_4` FOREIGN KEY (`ma_tien_muon`) REFERENCES `tien_muon_sach` (`ma_tien_muon`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sach_ibfk_5` FOREIGN KEY (`trang_thai`) REFERENCES `trang_thai_sach` (`ma_tt`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `the`
--
ALTER TABLE `the`
  ADD CONSTRAINT `the_ibfk_1` FOREIGN KEY (`ma_doc_gia`) REFERENCES `doc_gia` (`ma_doc_gia`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
