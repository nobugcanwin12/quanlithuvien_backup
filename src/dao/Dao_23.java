package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Sach;

public class Dao_23 {

    public List<Sach> getDSSach() {
        List<Sach> s = new ArrayList<Sach>();

        Connection conn = JDBCConnection.getJDBCConnection();

        String sql = "SELECT * FROM sach";

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery(sql);
            while (rs.next()) {
                Sach sach = new Sach();

                sach.setId(rs.getInt("id_sach"));
                sach.setTen(rs.getString("ten_sach"));
                sach.setMaTacGia(rs.getInt("ma_tac_gia"));
                sach.setMaTheLoai(rs.getInt("ma_the_loai"));
                sach.setMaNXB(rs.getInt("ma_nxb"));
                sach.setNamXB(rs.getInt("nam_xuat_ban"));

                sach.setTrangThai(rs.getInt("trang_thai"));

                s.add(sach);

            }
        } catch (SQLException e) {

        }
        return s;
    }

    public List<Sach> getBookByTitle(String tenSach) throws SQLException {
        List<Sach> s = new ArrayList<Sach>();
        Connection con = JDBCConnection.getJDBCConnection();
        Statement stm = con.createStatement();
        String sql = "SELECT * FROM sach WHERE ten_sach like '%" + tenSach + "%'";
        ResultSet rs = stm.executeQuery(sql);

        while (rs.next()) {
            Sach sach = new Sach();
            sach.setId(rs.getInt("id_sach"));
            sach.setTen(rs.getString("ten_sach"));
            sach.setTrangThai(rs.getInt("trang_thai"));

            s.add(sach);
        }

        return s;
    }

    public List<Sach> setListSach(ResultSet rs) throws SQLException {
        List<Sach> s = new ArrayList<Sach>();
        while (rs.next()) {
            Sach sach = new Sach();
            sach.setId(rs.getInt("id_sach"));
            sach.setTen(rs.getString("ten_sach"));
            sach.setMaTacGia(rs.getInt("ma_tac_gia"));
            sach.setMaTheLoai(rs.getInt("ma_the_loai"));
            sach.setMaNXB(rs.getInt("ma_nxb"));
            sach.setNamXB(rs.getInt("nam_xuat_ban"));
            sach.setMaTienMuon(rs.getInt("ma_tien_muon"));
            sach.setTrangThai(rs.getInt("trang_thai"));

            s.add(sach);
        }
        return s;
    }

    public List<Sach> getTheLoaiSach(String type) {
        List<Sach> s = new ArrayList<Sach>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "select * from sach, the_loai where ten_the_loai = ?"
                + " and sach.ma_the_loai = the_loai.ma_the_loai";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, type);
            ResultSet rs = ps.executeQuery();
            s = setListSach(rs);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return s;
    }

    public List<String> getAllTheLoai_16() {
        List<String> theLoais = new ArrayList<String>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT ten_the_loai FROM the_loai";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                String theloai = rs.getString("ten_the_loai");
                theLoais.add(theloai);
            }
            return theLoais;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public String getTrangThaiByMaTrangThai(int mtt) {
        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT ten_tt FROM trang_thai_sach WHERE ma_tt = ?";

        PreparedStatement pstm;
        try {
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, mtt);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            return rs.getString("ten_tt");
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
