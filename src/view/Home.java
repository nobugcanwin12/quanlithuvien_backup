package view;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import model.NguoiDung;
import model.Sach;
import service.Service_23;

public class Home extends javax.swing.JFrame {

    List<Sach> sach = new ArrayList<Sach>();
    Service_23 sachService_23;
    DefaultTableModel defaultTableModel;
    NguoiDung nguoiDung = null;
    boolean isLoaded = false;
    //các biến sách mượn
    public Sach sachMuon;
    DefaultTableModel DSMuon_model;

    public Home(NguoiDung user, int page) {

        sachService_23 = new Service_23();


        initComponents();
        setLocationRelativeTo(null);

        showPage(1);

        isLoaded = true;
    }

    public void showPage(int page) {
        hiddenPN();
        switch (page) {
            case 1:
                lblmuonSach.setBackground(new Color(101, 157, 189));
                MuonSach_PN.setVisible(true);
                muonSachPN();
                break;
            case 2:
                lblThe.setBackground(new Color(101, 157, 189));
                The_PN.setVisible(true);
                thePN();
                break;
            case 3:
                lblNguoiDung.setBackground(new Color(101, 157, 189));
                NguoiDung_PN.setVisible(true);
                nguoiDungPN();
                break;
            case 4:
                lblQLSach.setBackground(new Color(101, 157, 189));
                QLSach_PN.setVisible(true);
                qlSachPN();
                break;
            case 5:
                lblQLMuonTra.setBackground(new Color(101, 157, 189));
                QLMuonTra_PN.setVisible(true);
                quanLiMuonTraPN();
                break;
            case 6:
                lblTK.setBackground(new Color(101, 157, 189));
                TaiKhoan_PN.setVisible(true);
                taiKhoanPN();
                break;
            case 7:
                lblQLNhanVien.setBackground(new Color(101, 157, 189));
                QLNhanVien_PN.setVisible(true);
                qlNhanVienPN();
                break;
            case 8:
                lblThongKe.setBackground(new Color(101, 157, 189));
                ThongKe_PN.setVisible(true);
                thongKePN();
                break;
        }
    }

    public void muonSachPN() {
        defaultTableModel = new DefaultTableModel() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblSachTV.setModel(defaultTableModel);

        //them cot cho bookTB
        defaultTableModel.addColumn("Mã sách");
        defaultTableModel.addColumn("Tên sách");
        defaultTableModel.addColumn("Trạng thái");

        //them du lieu vao bookTB
        addRowShowSach(sachService_23.getDSSach());
        loadmaTheLoai();
        widthColumnTB();
        isLoaded = true;
        //sách mượn
        DSMuon_model = new DefaultTableModel() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblSachDaMuon.setModel(DSMuon_model);
        DSMuon_model.addColumn("Mã sách");
        DSMuon_model.addColumn("Tên sách");
        DSMuon_model.addColumn("Trạng thái");       
        addRowSachMuon(sach);                

        isLoaded = true;
        //sách mượn
        DSMuon_model = new DefaultTableModel();
        tblSachDaMuon.setModel(DSMuon_model);
        DSMuon_model.addColumn("Mã sách");
        DSMuon_model.addColumn("Tên sách");
        addRowSachMuon(sach);

        widthColumnTB();
        widthColumnTB_Muon();
        
        btnThemSachMuon.setEnabled(false);
    }

    public void thePN() {

    }

    public void nguoiDungPN() {

    }

    public void qlSachPN() {

    }

    public void quanLiMuonTraPN() {
        JTableHeader tus = hoaDonTB.getTableHeader();
        tus.setBackground(Color.red);
        tus.setSize(50, 100);
        defaultTableModel = new DefaultTableModel();
        hoaDonTB.setModel(defaultTableModel);
        defaultTableModel.addColumn("tú");
    }

    public void taiKhoanPN() {

    }

    public void qlNhanVienPN() {

    }

    public void thongKePN() {

    }

    final void addRowShowSach(List<Sach> sachs) {
        defaultTableModel.setRowCount(0);
        for (Sach sach : sachs) {
            String trangThai = sachService_23.getTrangThaiByMaTrangThai(sach.getTrangThai());
            defaultTableModel.addRow(new Object[]{sach.getId(), sach.getTen(), trangThai});
        }
    }

    //đổ dữ liệu ra bảng sách mượn
    final void addRowSachMuon(List<Sach> sachs) {
        DSMuon_model.setRowCount(0);
        for (Sach sach : sachs) {
            DSMuon_model.addRow(new Object[]{sach.getId(), sach.getTen()});
        }
    }

    public void loadmaTheLoai() {
        if (!isLoaded) {
            cbbTheLoai.removeAllItems();
            cbbTheLoai.addItem("Thể loại");
            List<String> theLoais = sachService_23.getAllTheLoai_16();
            for (String theLoai : theLoais) {
                cbbTheLoai.addItem(theLoai);
            }
        }
    }

    public void hiddenPN() {
        MuonSach_PN.setVisible(false);
        The_PN.setVisible(false);
        NguoiDung_PN.setVisible(false);
        QLSach_PN.setVisible(false);
        ThongKe_PN.setVisible(false);
        TaiKhoan_PN.setVisible(false);
        QLNhanVien_PN.setVisible(false);
        QLMuonTra_PN.setVisible(false);

        lblmuonSach.setBackground(new Color(255, 255, 255));
        lblThe.setBackground(new Color(255, 255, 255));
        lblNguoiDung.setBackground(new Color(255, 255, 255));
        lblQLSach.setBackground(new Color(255, 255, 255));
        lblTK.setBackground(new Color(255, 255, 255));
        lblThongKe.setBackground(new Color(255, 255, 255));
        lblQLNhanVien.setBackground(new Color(255, 255, 255));
        lblQLMuonTra.setBackground(new Color(255, 255, 255));
    }
    

    public void widthColumnTB() {
        TableColumnModel columnModel = tblSachTV.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(70);
        columnModel.getColumn(0).setMaxWidth(70);
        columnModel.getColumn(1).setPreferredWidth(320);
        columnModel.getColumn(1).setMaxWidth(320);
        columnModel.getColumn(2).setPreferredWidth(70);
        columnModel.getColumn(2).setMaxWidth(70);
    }
    
    public void widthColumnTB_Muon() {
        TableColumnModel columnModel = tblSachDaMuon.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(80);
        columnModel.getColumn(0).setMaxWidth(80);
        columnModel.getColumn(1).setPreferredWidth(420);
        columnModel.getColumn(1).setMaxWidth(420);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel11 = new javax.swing.JPanel();
        sideBar_PN = new javax.swing.JPanel();
        lblmuonSach = new javax.swing.JLabel();
        lblThe = new javax.swing.JLabel();
        lblNguoiDung = new javax.swing.JLabel();
        lblQLSach = new javax.swing.JLabel();
        lblTK = new javax.swing.JLabel();
        lblQLNhanVien = new javax.swing.JLabel();
        lblThongKe = new javax.swing.JLabel();
        lblQLMuonTra = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        MuonSach_PN = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        txtSearch = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        lbGioiHan = new javax.swing.JPanel();
        btnThanhToan = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblSachTV = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblSachDaMuon = new javax.swing.JTable();
        btnThemSachMuon = new javax.swing.JButton();
        btnXoaSachMuon = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox<>();
        lbTongMuon = new javax.swing.JLabel();
        lblGioiHan = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        cbbTheLoai = new javax.swing.JComboBox<>();
        The_PN = new javax.swing.JPanel();
        NguoiDung_PN = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        nguoidung_ma_PN = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        nguoidung_add_51 = new javax.swing.JTextField();
        NguoiDung_Ten_PN = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        nguoidung_addTen_51 = new javax.swing.JTextField();
        NguoiDung_DiaChi_PN = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        nguoidung_addDiaChi_51 = new javax.swing.JTextField();
        NguoiDung_sdt_PN = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        nguoidung_addSdt_51 = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        nguoidung_sdtTK_51 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        QLSach_PN = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        ThongKe_PN = new javax.swing.JPanel();
        QLNhanVien_PN = new javax.swing.JPanel();
        QLMuonTra_PN = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox<>();
        txtSearch1 = new javax.swing.JTextField();
        btnSearch1 = new javax.swing.JButton();
        lbGioiHan1 = new javax.swing.JPanel();
        lbTongMuon1 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        hoaDonTB = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        TaiKhoan_PN = new javax.swing.JPanel();

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Trang chủ");
        setResizable(false);

        lblmuonSach.setBackground(new java.awt.Color(101, 157, 189));
        lblmuonSach.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblmuonSach.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblmuonSach.setText("Trang mượn sách");
        lblmuonSach.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblmuonSach.setOpaque(true);
        lblmuonSach.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblmuonSachMouseClicked(evt);
            }
        });

        lblThe.setBackground(new java.awt.Color(255, 255, 255));
        lblThe.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblThe.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblThe.setText("Thẻ");
        lblThe.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblThe.setOpaque(true);
        lblThe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblTheMouseClicked(evt);
            }
        });

        lblNguoiDung.setBackground(new java.awt.Color(255, 255, 255));
        lblNguoiDung.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblNguoiDung.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNguoiDung.setText("Khách hàng");
        lblNguoiDung.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblNguoiDung.setOpaque(true);
        lblNguoiDung.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblNguoiDungMouseClicked(evt);
            }
        });

        lblQLSach.setBackground(new java.awt.Color(255, 255, 255));
        lblQLSach.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblQLSach.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQLSach.setText("Quản lý sách");
        lblQLSach.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQLSach.setOpaque(true);
        lblQLSach.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblQLSachMouseClicked(evt);
            }
        });

        lblTK.setBackground(new java.awt.Color(255, 255, 255));
        lblTK.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblTK.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTK.setText("Tài khoản");
        lblTK.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblTK.setOpaque(true);
        lblTK.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblTKMouseClicked(evt);
            }
        });

        lblQLNhanVien.setBackground(new java.awt.Color(255, 255, 255));
        lblQLNhanVien.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblQLNhanVien.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQLNhanVien.setText("Quản lý nhân viên");
        lblQLNhanVien.setToolTipText("");
        lblQLNhanVien.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQLNhanVien.setOpaque(true);
        lblQLNhanVien.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblQLNhanVienMouseClicked(evt);
            }
        });

        lblThongKe.setBackground(new java.awt.Color(255, 255, 255));
        lblThongKe.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblThongKe.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblThongKe.setText("Thống kê");
        lblThongKe.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblThongKe.setOpaque(true);
        lblThongKe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblThongKeMouseClicked(evt);
            }
        });

        lblQLMuonTra.setBackground(new java.awt.Color(255, 255, 255));
        lblQLMuonTra.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblQLMuonTra.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQLMuonTra.setText("Quản lý mượn trả");
        lblQLMuonTra.setToolTipText("");
        lblQLMuonTra.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQLMuonTra.setOpaque(true);
        lblQLMuonTra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblQLMuonTraMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout sideBar_PNLayout = new javax.swing.GroupLayout(sideBar_PN);
        sideBar_PN.setLayout(sideBar_PNLayout);
        sideBar_PNLayout.setHorizontalGroup(
            sideBar_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblmuonSach, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
            .addComponent(lblNguoiDung, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblThe, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblQLSach, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblTK, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblThongKe, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblQLMuonTra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblQLNhanVien, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        sideBar_PNLayout.setVerticalGroup(
            sideBar_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sideBar_PNLayout.createSequentialGroup()
                .addComponent(lblmuonSach, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(lblThe, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(lblNguoiDung, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(lblQLSach, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(lblQLMuonTra, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(lblTK, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblQLNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(lblThongKe, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        MuonSach_PN.setBackground(new java.awt.Color(255, 51, 51));
        MuonSach_PN.setLayout(new java.awt.CardLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 204));

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel8.setText("Tìm kiếm:");

        jComboBox1.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả sách", "Sách còn trong thư viện" }));

        txtSearch.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
        });

        btnSearch.setBackground(new java.awt.Color(204, 204, 255));
        btnSearch.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Search-icon.png"))); // NOI18N
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        lbGioiHan.setBackground(new java.awt.Color(148, 176, 183));
        lbGioiHan.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                lbGioiHanAncestorAdded(evt);
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        btnThanhToan.setBackground(new java.awt.Color(204, 204, 255));
        btnThanhToan.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        btnThanhToan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Pay-icon.png"))); // NOI18N
        btnThanhToan.setText("Thanh Toán");

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Sách thư viện", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 15))); // NOI18N

        tblSachTV.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        tblSachTV.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblSachTV.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tblSachTV.setRowHeight(30);
        tblSachTV.setSelectionBackground(new java.awt.Color(58, 175, 169));
        tblSachTV.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblSachTV.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblSachTVMouseClicked(evt);
            }
        });
        jScrollPane7.setViewportView(tblSachTV);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Sách cho mượn", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 15))); // NOI18N

        tblSachDaMuon.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        tblSachDaMuon.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblSachDaMuon.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tblSachDaMuon.setRowHeight(30);
        tblSachDaMuon.setSelectionBackground(new java.awt.Color(58, 175, 169));
        tblSachDaMuon.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane5.setViewportView(tblSachDaMuon);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 503, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnThemSachMuon.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        btnThemSachMuon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/addbook-icon.png"))); // NOI18N
        btnThemSachMuon.setText("Mượn");
        btnThemSachMuon.setEnabled(false);
        btnThemSachMuon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemSachMuonActionPerformed(evt);
            }
        });

        btnXoaSachMuon.setBackground(new java.awt.Color(204, 204, 255));
        btnXoaSachMuon.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        btnXoaSachMuon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/xoa.png"))); // NOI18N
        btnXoaSachMuon.setText("Xóa");
        btnXoaSachMuon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaSachMuonActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel9.setText("Tổng lượt mượn:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Calendar-icon.png"))); // NOI18N
        jLabel10.setText("Chọn ngày hẹn trả:");

        jComboBox2.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "7", "14", "20", "30", "50", "90" }));

        lbTongMuon.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N

        lblGioiHan.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lblGioiHan.setText("0");

        jLabel12.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel12.setText("/");

        jLabel13.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel13.setText("3");

        jSeparator2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jSeparator2.setOpaque(true);

        javax.swing.GroupLayout lbGioiHanLayout = new javax.swing.GroupLayout(lbGioiHan);
        lbGioiHan.setLayout(lbGioiHanLayout);
        lbGioiHanLayout.setHorizontalGroup(
            lbGioiHanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lbGioiHanLayout.createSequentialGroup()
                .addGroup(lbGioiHanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(lbGioiHanLayout.createSequentialGroup()
                        .addGap(1549, 1549, 1549)
                        .addComponent(lbTongMuon, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(lbGioiHanLayout.createSequentialGroup()
                        .addGap(661, 661, 661)
                        .addComponent(jLabel10)
                        .addGap(37, 37, 37)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(lbGioiHanLayout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(lbGioiHanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnThanhToan)
                            .addGroup(lbGioiHanLayout.createSequentialGroup()
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnThemSachMuon)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(lbGioiHanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(lbGioiHanLayout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addGap(18, 18, 18)
                                        .addComponent(lblGioiHan, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnXoaSachMuon))
                                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        lbGioiHanLayout.setVerticalGroup(
            lbGioiHanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lbGioiHanLayout.createSequentialGroup()
                .addGroup(lbGioiHanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(lbGioiHanLayout.createSequentialGroup()
                        .addGroup(lbGioiHanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(lbGioiHanLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, lbGioiHanLayout.createSequentialGroup()
                                .addContainerGap(46, Short.MAX_VALUE)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(10, 10, 10))
                    .addGroup(lbGioiHanLayout.createSequentialGroup()
                        .addGap(186, 186, 186)
                        .addComponent(btnThemSachMuon)))
                .addGap(12, 12, 12)
                .addGroup(lbGioiHanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblGioiHan, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnXoaSachMuon, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(lbGioiHanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnThanhToan, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbTongMuon, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        cbbTheLoai.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        cbbTheLoai.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbTheLoaiItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbGioiHan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48)
                        .addComponent(cbbTheLoai, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 958, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbbTheLoai, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSearch, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearch, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addComponent(lbGioiHan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 1225, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        MuonSach_PN.add(jPanel2, "card2");

        The_PN.setBackground(new java.awt.Color(251, 232, 166));
        The_PN.setLayout(new java.awt.CardLayout());

        NguoiDung_PN.setBackground(new java.awt.Color(135, 206, 235));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 3, 48)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Thêm Người  Dùng");

        jPanel8.setBackground(new java.awt.Color(135, 206, 235));
        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.lightGray, java.awt.Color.lightGray), "Xử lý", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Times New Roman", 0, 18))); // NOI18N

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Save-as.png"))); // NOI18N
        jButton2.setText("Lưu");

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/2_004.png"))); // NOI18N
        jButton3.setText("Xóa");

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit-icon.png"))); // NOI18N
        jButton4.setText("Sửa");

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Add-Male-User.png"))); // NOI18N
        jButton5.setText("Thêm");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(567, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 47, Short.MAX_VALUE))
        );

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thông tin người dùng", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 0, 18))); // NOI18N
        jPanel9.setLayout(new java.awt.GridLayout(0, 1));

        jPanel10.setLayout(new java.awt.GridLayout(0, 1, 10, 30));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Mã Đọc Giả :");

        nguoidung_add_51.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        javax.swing.GroupLayout nguoidung_ma_PNLayout = new javax.swing.GroupLayout(nguoidung_ma_PN);
        nguoidung_ma_PN.setLayout(nguoidung_ma_PNLayout);
        nguoidung_ma_PNLayout.setHorizontalGroup(
            nguoidung_ma_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(nguoidung_ma_PNLayout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nguoidung_add_51, javax.swing.GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE))
        );
        nguoidung_ma_PNLayout.setVerticalGroup(
            nguoidung_ma_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(nguoidung_add_51, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel10.add(nguoidung_ma_PN);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Tên :");

        nguoidung_addTen_51.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        javax.swing.GroupLayout NguoiDung_Ten_PNLayout = new javax.swing.GroupLayout(NguoiDung_Ten_PN);
        NguoiDung_Ten_PN.setLayout(NguoiDung_Ten_PNLayout);
        NguoiDung_Ten_PNLayout.setHorizontalGroup(
            NguoiDung_Ten_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NguoiDung_Ten_PNLayout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nguoidung_addTen_51, javax.swing.GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE))
        );
        NguoiDung_Ten_PNLayout.setVerticalGroup(
            NguoiDung_Ten_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(nguoidung_addTen_51, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
        );

        jPanel10.add(NguoiDung_Ten_PN);

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Địa Chỉ : ");

        nguoidung_addDiaChi_51.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        javax.swing.GroupLayout NguoiDung_DiaChi_PNLayout = new javax.swing.GroupLayout(NguoiDung_DiaChi_PN);
        NguoiDung_DiaChi_PN.setLayout(NguoiDung_DiaChi_PNLayout);
        NguoiDung_DiaChi_PNLayout.setHorizontalGroup(
            NguoiDung_DiaChi_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NguoiDung_DiaChi_PNLayout.createSequentialGroup()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nguoidung_addDiaChi_51, javax.swing.GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE))
        );
        NguoiDung_DiaChi_PNLayout.setVerticalGroup(
            NguoiDung_DiaChi_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NguoiDung_DiaChi_PNLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(nguoidung_addDiaChi_51, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
        );

        jPanel10.add(NguoiDung_DiaChi_PN);

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Số điện thoại :");

        nguoidung_addSdt_51.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        javax.swing.GroupLayout NguoiDung_sdt_PNLayout = new javax.swing.GroupLayout(NguoiDung_sdt_PN);
        NguoiDung_sdt_PN.setLayout(NguoiDung_sdt_PNLayout);
        NguoiDung_sdt_PNLayout.setHorizontalGroup(
            NguoiDung_sdt_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NguoiDung_sdt_PNLayout.createSequentialGroup()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(nguoidung_addSdt_51, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NguoiDung_sdt_PNLayout.setVerticalGroup(
            NguoiDung_sdt_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(nguoidung_addSdt_51, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
        );

        jPanel10.add(NguoiDung_sdt_PN);

        jPanel9.add(jPanel10);

        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Bảng thông tin khách hàng", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Times New Roman", 0, 18))); // NOI18N

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("SDT :");

        nguoidung_sdtTK_51.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Mã KH", "Tên KH", "Địa Chỉ", "SDT"
            }
        ));
        jTable1.setColumnSelectionAllowed(true);
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nguoidung_sdtTK_51, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jScrollPane1)
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nguoidung_sdtTK_51, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout NguoiDung_PNLayout = new javax.swing.GroupLayout(NguoiDung_PN);
        NguoiDung_PN.setLayout(NguoiDung_PNLayout);
        NguoiDung_PNLayout.setHorizontalGroup(
            NguoiDung_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1225, Short.MAX_VALUE)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(NguoiDung_PNLayout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        NguoiDung_PNLayout.setVerticalGroup(
            NguoiDung_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NguoiDung_PNLayout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(NguoiDung_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        QLSach_PN.setLayout(new java.awt.CardLayout());

        jPanel5.setBackground(new java.awt.Color(136, 189, 188));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1225, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 719, Short.MAX_VALUE)
        );

        QLSach_PN.add(jPanel5, "card2");

        ThongKe_PN.setLayout(new java.awt.CardLayout());

        QLNhanVien_PN.setLayout(new java.awt.CardLayout());

        QLMuonTra_PN.setLayout(new java.awt.CardLayout());

        jPanel7.setBackground(new java.awt.Color(255, 255, 204));
        jPanel7.setPreferredSize(new java.awt.Dimension(1225, 720));

        jLabel11.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel11.setText("Tìm kiếm:");

        jComboBox3.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả", "Đã trả hết sách", "Đã hết hạn mượn" }));

        txtSearch1.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        txtSearch1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearch1KeyPressed(evt);
            }
        });

        btnSearch1.setBackground(new java.awt.Color(204, 204, 255));
        btnSearch1.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        btnSearch1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Search-icon.png"))); // NOI18N
        btnSearch1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearch1ActionPerformed(evt);
            }
        });

        lbGioiHan1.setBackground(new java.awt.Color(148, 176, 183));
        lbGioiHan1.setPreferredSize(new java.awt.Dimension(1225, 596));
        lbGioiHan1.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                lbGioiHan1AncestorAdded(evt);
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        lbTongMuon1.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N

        hoaDonTB.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        hoaDonTB.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1", "2", "3", "4"},
                {"2", "2", "3", "4"}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        hoaDonTB.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        hoaDonTB.setRowHeight(50);
        hoaDonTB.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane8.setViewportView(hoaDonTB);

        jButton1.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/view-detail.png"))); // NOI18N
        jButton1.setText("Xem chi tiết");

        javax.swing.GroupLayout lbGioiHan1Layout = new javax.swing.GroupLayout(lbGioiHan1);
        lbGioiHan1.setLayout(lbGioiHan1Layout);
        lbGioiHan1Layout.setHorizontalGroup(
            lbGioiHan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lbGioiHan1Layout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(lbGioiHan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 1064, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(80, 80, 80)
                .addComponent(lbTongMuon1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        lbGioiHan1Layout.setVerticalGroup(
            lbGioiHan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lbGioiHan1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 524, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(lbGioiHan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbTongMuon1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lbGioiHan1Layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        jLabel14.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel14.setText("Lọc:");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel11)
                    .addComponent(jLabel14))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(txtSearch1)
                        .addGap(18, 18, 18)
                        .addComponent(btnSearch1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(80, 80, 80))
            .addComponent(lbGioiHan1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSearch1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtSearch1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(lbGioiHan1, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE))
        );

        QLMuonTra_PN.add(jPanel7, "card2");

        TaiKhoan_PN.setLayout(new java.awt.CardLayout());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(MuonSach_PN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(The_PN, javax.swing.GroupLayout.PREFERRED_SIZE, 1225, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(NguoiDung_PN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(QLSach_PN, javax.swing.GroupLayout.PREFERRED_SIZE, 1225, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(ThongKe_PN, javax.swing.GroupLayout.DEFAULT_SIZE, 1225, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(QLNhanVien_PN, javax.swing.GroupLayout.DEFAULT_SIZE, 1225, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(QLMuonTra_PN, javax.swing.GroupLayout.PREFERRED_SIZE, 1225, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(TaiKhoan_PN, javax.swing.GroupLayout.PREFERRED_SIZE, 1225, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MuonSach_PN, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(The_PN, javax.swing.GroupLayout.PREFERRED_SIZE, 719, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(NguoiDung_PN, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(QLSach_PN, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(ThongKe_PN, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(QLNhanVien_PN, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(QLMuonTra_PN, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(TaiKhoan_PN, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(sideBar_PN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 1225, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(0, 190, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1225, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(sideBar_PN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 1, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblmuonSachMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblmuonSachMouseClicked
        showPage(1);
    }//GEN-LAST:event_lblmuonSachMouseClicked

    private void lblTheMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTheMouseClicked
        showPage(2);
    }//GEN-LAST:event_lblTheMouseClicked

    private void lblNguoiDungMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblNguoiDungMouseClicked
        showPage(3);
    }//GEN-LAST:event_lblNguoiDungMouseClicked

    private void lblQLSachMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblQLSachMouseClicked
        showPage(4);
    }//GEN-LAST:event_lblQLSachMouseClicked

    private void lblTKMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTKMouseClicked
        showPage(6);
    }//GEN-LAST:event_lblTKMouseClicked

    private void lblQLNhanVienMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblQLNhanVienMouseClicked
        showPage(7);
    }//GEN-LAST:event_lblQLNhanVienMouseClicked

    private void lblThongKeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblThongKeMouseClicked
        showPage(8);
    }//GEN-LAST:event_lblThongKeMouseClicked

    private void lblQLMuonTraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblQLMuonTraMouseClicked
        showPage(5);
    }//GEN-LAST:event_lblQLMuonTraMouseClicked

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                addRowShowSach(sachService_23.getBookByTitle(txtSearch.getText()));
                txtSearch.setText("");
            } catch (SQLException ex) {
                Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_txtSearchKeyPressed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        try {
            addRowShowSach(sachService_23.getBookByTitle(txtSearch.getText()));
            txtSearch.setText("");
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnThemSachMuonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemSachMuonActionPerformed
        Sach s = new Sach();
        s.setId(Integer.parseInt(tblSachTV.getValueAt(tblSachTV.getSelectedRow(), 0).toString()));
        s.setTen(tblSachTV.getValueAt(tblSachTV.getSelectedRow(), 1).toString());
        sach.add(s);
        addRowSachMuon(sach);

        lblGioiHan.setText(String.valueOf(sach.size()));
        if (sach.size() > 3) {
            lblGioiHan.setForeground(Color.red);
            btnThemSachMuon.setEnabled(false);
            JOptionPane.showMessageDialog(this, "Vượt quá số sách cho phép mượn!", "Lỗi", JOptionPane.ERROR_MESSAGE);
            btnThanhToan.setEnabled(false);
        } else {
            lblGioiHan.setForeground(Color.blue);
        }
    }//GEN-LAST:event_btnThemSachMuonActionPerformed

    private void btnXoaSachMuonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaSachMuonActionPerformed
        int row = tblSachDaMuon.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Chọn một cuốn sách để xóa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            sach.remove(row);
            addRowSachMuon(sach);
        }
        lblGioiHan.setText(String.valueOf(sach.size()));
        if (sach.size() == 3) {
            lblGioiHan.setForeground(Color.blue);
            btnThemSachMuon.setEnabled(true);
            btnThanhToan.setEnabled(true);
        } else {
            lblGioiHan.setForeground(Color.red);
        }
    }//GEN-LAST:event_btnXoaSachMuonActionPerformed

    private void lbGioiHanAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_lbGioiHanAncestorAdded
        //
        //
    }//GEN-LAST:event_lbGioiHanAncestorAdded

    private void cbbTheLoaiItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbTheLoaiItemStateChanged
        String tlSelected = cbbTheLoai.getSelectedItem().toString();
        if (tlSelected.equals("Thể loại") == true) {
            addRowShowSach(sachService_23.getDSSach());
        } else {
            addRowShowSach(sachService_23.getTheLoaiSach(tlSelected));
        }
    }//GEN-LAST:event_cbbTheLoaiItemStateChanged

    private void lbGioiHan1AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_lbGioiHan1AncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_lbGioiHan1AncestorAdded

    private void btnSearch1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearch1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSearch1ActionPerformed

    private void txtSearch1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearch1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearch1KeyPressed

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void tblSachTVMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSachTVMouseClicked
        int row = tblSachTV.getSelectedRow();
        if(row != -1) {
            btnThemSachMuon.setEnabled(true);
        } else {
            btnThemSachMuon.setEnabled(false);
        }
    }//GEN-LAST:event_tblSachTVMouseClicked

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed

  
    }//GEN-LAST:event_jButton5ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel MuonSach_PN;
    private javax.swing.JPanel NguoiDung_DiaChi_PN;
    private javax.swing.JPanel NguoiDung_PN;
    private javax.swing.JPanel NguoiDung_Ten_PN;
    private javax.swing.JPanel NguoiDung_sdt_PN;
    private javax.swing.JPanel QLMuonTra_PN;
    private javax.swing.JPanel QLNhanVien_PN;
    private javax.swing.JPanel QLSach_PN;
    private javax.swing.JPanel TaiKhoan_PN;
    private javax.swing.JPanel The_PN;
    private javax.swing.JPanel ThongKe_PN;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnSearch1;
    private javax.swing.JButton btnThanhToan;
    private javax.swing.JButton btnThemSachMuon;
    private javax.swing.JButton btnXoaSachMuon;
    private javax.swing.JComboBox<String> cbbTheLoai;
    private javax.swing.JTable hoaDonTB;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable jTable1;
    private javax.swing.JPanel lbGioiHan;
    private javax.swing.JPanel lbGioiHan1;
    private javax.swing.JLabel lbTongMuon;
    private javax.swing.JLabel lbTongMuon1;
    private javax.swing.JLabel lblGioiHan;
    private javax.swing.JLabel lblNguoiDung;
    private javax.swing.JLabel lblQLMuonTra;
    private javax.swing.JLabel lblQLNhanVien;
    private javax.swing.JLabel lblQLSach;
    private javax.swing.JLabel lblTK;
    private javax.swing.JLabel lblThe;
    private javax.swing.JLabel lblThongKe;
    private javax.swing.JLabel lblmuonSach;
    private javax.swing.JTextField nguoidung_addDiaChi_51;
    private javax.swing.JTextField nguoidung_addSdt_51;
    private javax.swing.JTextField nguoidung_addTen_51;
    private javax.swing.JTextField nguoidung_add_51;
    private javax.swing.JPanel nguoidung_ma_PN;
    private javax.swing.JTextField nguoidung_sdtTK_51;
    private javax.swing.JPanel sideBar_PN;
    private javax.swing.JTable tblSachDaMuon;
    private javax.swing.JTable tblSachTV;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtSearch1;
    // End of variables declaration//GEN-END:variables
}
